import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: 'main',
    redirect: '/courses',
  },
  {
    path: "/registration",
    name: "registration",
    component: () => import("../pages/Registration.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../pages/Login.vue"),
  },
  {
    path: "/settings",
    name: "settings",
    component: () => import("../pages/Settings.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/courses",
    name: "course_catalog",
    component: () => import("../pages/CourseCatalog.vue"),
  },
  {
    path: '/courses/:id',
    name: 'course_preview',
    component: () => import('../pages/CoursePreview.vue'),
  },
  {
    path: '/my_courses/:id/day/:day',
    name: 'course_page',
    component: () => import('../pages/CoursePage.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/courses/:id/payment',
    name: 'course_payment',
    component: () => import('../pages/CoursePayment.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/my_courses',
    name: 'my_courses',
    component: () => import('../pages/MyCourses.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '*',
    name: 'not_found',
    component: () => import('../pages/NotFound.vue'),
  }
];

const router = new VueRouter({
  mode: "history",
  routes: routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!window.$cookies.isKey("auth_token")) {
      next({ name: "login" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
