import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import ApiPlugin from "./plugins/api";
Vue.use(ApiPlugin);

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

import VueCookies from "vue-cookies";
Vue.use(VueCookies);

import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

import VueYoutube from "vue-youtube";
Vue.use(VueYoutube);

import VueFlashMessage from "vue-flash-message";
Vue.use(VueFlashMessage);

import VueMarkdown from "vue-markdown";
Vue.component('vue-markdown', VueMarkdown);

Vue.config.productionTip = false;

import "./global.css";

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
