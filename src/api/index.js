import instance from "./instance";
import authModule from "./auth";
import platformModule from "./platform";

export default {
  auth: authModule(instance),
  platform: platformModule(instance),
};
