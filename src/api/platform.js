export default function(instance) {
  return {
    get_all_courses(search_query) {
      if (search_query === undefined) {
        return instance.get('/api/courses/');
      }
      return instance.get('/api/courses/', {
        params: { 
          search: search_query
        }
      });
    },
    get_course_by_id(id) {
      return instance.get('/api/courses/' + id);
    },
    get_day_by_course_id(course_id, day_num) {
      return instance.get('/api/courses/' + course_id + '/blocks/' + day_num);
    },
    change_exercise_completion(exercise_id) {
      return instance.post('/api/courses/exercise_action/' + exercise_id);
    },
    get_my_courses(search_query) {
      if (search_query === undefined) {
        return instance.get('/api/courses/my_courses/');
      }
      return instance.get('/api/courses/my_courses/', {
        params: { 
          search: search_query
        }
      });
    },
    create_course_payment(course_id, payload) {
      return instance.post('/api/courses/' + course_id + '/pay/create/', payload);
    }
  }
}