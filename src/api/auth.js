export default function(instance) {
  return {
    registration(payload) {
      return instance.post('/api/auth/registration/', payload);
    },
    login(payload) {
      return instance.post('/api/auth/login/', payload);
    },
    change_user_settings(payload) {
      return instance.post('/api/auth/user_settings/', payload);
    }
  }
}